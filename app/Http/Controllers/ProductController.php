<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    public function home (){
        return view('create');
        // direturn based on blabla.blade.php...resources -> views
    }

    public function store(Request $request){
        // Product::create([
        //     'product_name' => $request->product_name,
        //     //sebelah kiri : nama tabel, yg kanan diambil dari tag input kita yang ada di create.blade.php
        //     //Product dari provider (tabel)
        // //$request untuk membaca tag2 input didalam form
        //     'price' => $request->price,
        //     'stock' => $request->stock,
        // ]);
        Product::create($request->all()); 
        // ini dipake bila urutan dari inputan udah sesuai sama urutan yg di tabel
        
        // return back();
        return redirect('/products');   
    }

    public function viewProduct (){
        $products = Product::all();
        return view('products', compact('products'));
        // compact = mempassing variable didalam $products = Product::all();
    }

    public function edit ($id){
        $product = Product::findOrFail($id);
        //findorfail untuk dapetin 1 aja 
        // $product = Product::where('id', $id)->first();
        // where untuk memfilter banyak data
        // dd($product); 
        return view('edit', compact('product'));
    }

    public function update(Request $request, $id){
        // Product::where('id', $id)->update([
        //     'product_name' => $request->product_name,
        //     'price' => $request->price,
        //     'stock' => $request->stock, 
        // ]);

        Product::findOrFail($id)->update($request->all());
        return redirect('/products');
    }

    public function delete($id){
        Product::destroy($id);
        return back();
    }
}
